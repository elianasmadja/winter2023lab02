public class Calculator {
    
    public static double add(double num1, double num2) {
        double addition = num1 + num2;
        return addition;
    }
    
    public static double subtract(double num1, double num2) {
        double substraction = num1 - num2;
        return substraction;
    }
    
    public double multiply(double num1, double num2) {
        double multiplication = num1 * num2;
        return multiplication;
    }
    
    public double divide(double num1, double num2) {
        double division = num1 / num2;
        return division;
    }
    
}