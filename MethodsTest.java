 public class MethodsTest{
    public static void main(String[]args){
        //int x = 5;
        //System.out.println(x);
        //methodNoInputNoReturn();
        /*System.out.println(x);
        methodOneInputNoReturn(x);
        System.out.println(x);*/
        //methodTwoInputNoReturn(5.5,3);
        //int z = methodNoInputReturnInt();
        //System.out.println("The value of z is " + z);
        //double squareRootOfNums = sumSquareRoot(9,5);
       //System.out.println("The answer to the equation is " + squareRootOfNums);
       /*String s1 = "java";
       String s2 = "programming";
       //String.length();
       System.out.println(s1.length());
       System.out.println(s2.length());*/
        
       //System.out.println(SecondClass.addOne(50));
       //System.out.println(SecondClass.addTwo(50));
       SecondClass sc = new SecondClass();
       System.out.println(sc.addTwo(50));
    
        
    }
    public static void methodNoInputNoReturn() {
        System.out.println("I'm in a method that takes no input and returns nothing");
        int x = 20; 
        System.out.println(x);
    }
     
    public static void methodOneInputNoReturn(int number) {
        System.out.println("Inside the method one input no return");
        number = number - 5;
        System.out.println(number);
        
    }
     
    public static void methodTwoInputNoReturn(int hello, double goodbye) {
        System.out.println("Your integer number is " + hello);
        System.out.println("Your double number is " + goodbye);
    }
    
    public static int methodNoInputReturnInt(){
        int num = 5;
        return num;
    }
     
    public static double sumSquareRoot(int num1, int num2) {
        int sumOfNums = num1 + num2;
        double numsSqaured = Math.sqrt(sumOfNums);
        return numsSqaured ;
    }
}