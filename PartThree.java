import java.util.Scanner;
public class PartThree{
    
    public static void main(String[]args) {
        double firstNum = 0.0;
        double secondNum = 0.0;
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter your first number");
        firstNum = scan.nextDouble();
        System.out.println("Please enter your second number");
        secondNum = scan.nextDouble();
        
        System.out.println("The sum of your two numbers is " + (Calculator.add(firstNum, secondNum))); 
        
        System.out.println("The difference of your two numbers is " + (Calculator.subtract(firstNum, secondNum)));
        
        Calculator cal = new Calculator();
        
        System.out.println("The product of your two numbers is " + (cal.multiply(firstNum, secondNum)));
        
        System.out.println("The quotient of your two numbers is " + (cal.divide(firstNum, secondNum)));  
    }
    
    
}